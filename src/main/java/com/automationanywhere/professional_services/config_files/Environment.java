package com.automationanywhere.professional_services.config_files;

public enum Environment
{
    DEFAULT, DEV, UAT, PROD
}

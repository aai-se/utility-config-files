package com.automationanywhere.professional_services.config_files;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.professional_services.cr_apis.apis.enums.WorkspaceType;
import com.automationanywhere.professional_services.utilities.ControlRoomApiUtility;
import com.automationanywhere.professional_services.utilities.SerializationUtility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 This class is responsible for loading config files either from the Control Room repository or a designated network fileshare if one is available.
 */
public class ConfigFileLoader
{

    //region Constants for API file search filter defintions

    // filter to get exact match for "Automation Anywhere\Bots\Processes" folder
    public static final String FILTER_REQUEST_PROCESSES_FOLDER = "{\"filter\":{\"operator\":\"eq\",\"field\":\"path\",\"value\":\"Automation Anywhere\\\\Bots\\\\Processes\"}}";

    // filter to get matches for subfolders matching a specified process ID in the "Automation Anywhere\Bots\Processes" folder
    public static final String FILTER_REQUEST_PROCESS_SUBFOLDER = "{\"filter\":{\"operator\":\"and\",\"operands\":[{\"operator\":\"substring\",\"field\":\"path\"," +
                                                                          "\"value\":\"Processes\\\\[PROCESS_ID]\"},{\"operator\":\"eq\",\"field\":\"type\"," +
                                                                          "\"value\":\"application/vnd.aa.directory\"},{\"operator\":\"eq\",\"field\":\"parentId\"," +
                                                                          "\"value\":\"[PROCESS_FOLDER_ID]\"}]}}";

    // filter to get matches for subfolders containing the word "Assets" as a child of a specific folder
    public static final String FILTER_REQUEST_ASSETS_SUBFOLDER = "{\"filter\":{\"operator\":\"and\",\"operands\":[{\"operator\":\"substring\",\"field\":\"path\"," +
                                                                         "\"value\":\"Assets\"},{\"operator\":\"eq\",\"field\":\"type\",\"value\":\"application/vnd.aa" +
                                                                         ".directory\"},{\"operator\":\"eq\",\"field\":\"parentId\",\"value\":\"[PROCESS_SUBFOLDER_ID]\"}]}}";

    // filter to get matches for files with type "application/json" containing the words "config.json" as a child of a specific folder
    public static final String FILTER_REQUEST_CONFIG_FILE_JSON = "{\"filter\":{\"operator\":\"and\",\"operands\":[{\"operator\":\"substring\",\"field\":\"path\"," +
                                                                         "\"value\":\"config.json\"},{\"operator\":\"eq\",\"field\":\"type\",\"value\":\"application/json\"}," +
                                                                         "{\"operator\":\"eq\",\"field\":\"parentId\",\"value\":\"[ASSETS_FOLDER_ID]\"}]}}";
    //endregion

    //region Constants for API response regular expressions

    // process subfolder regex - logic:
    // 1) must match "Processes\\" then the process ID
    // 2) optionally:
    // 2.1) must be followed by at least one character that isn't a digit - ensures "Processes\12 - def" will not match when looking for "Processes\123 - abc"
    // 2.2) must not be followed by any backslashes before the end of the string - ensures "Processes\123 - abc\Assets" will not match when looking for "Processes\123 - abc"
    public static final String REGEX_PATTERN_PROCESS_SUBFOLDER = "(Processes\\\\)(PROCESS_ID)([^\\d][^\\\\]+)?$";

    // assets subfolder regex - logic:
    // must end with "\\Assets" - ensures will not match incorrectly for "\\Assets - temp"
    public static final String REGEX_PATTERN_ASSETS_SUBFOLDER = "(\\\\Assets)$";


    // config file regex - logic:
    // 1) must match "Assets\\" then the process ID followed by a character that isn't a digit - ensures "Assets\12 - copied config file" will not match incorrectly for
    // "Assets\123 - abc config file.json"
    // 2) must end with "config.json"
    public static final String REGEX_PATTERN_CONFIG_FILE = "(Assets\\\\)(PROCESS_ID)[^\\d].*(config\\.json)$";
    //endregion

    /**
     Public method to load a config file.

     @param processId  Unique ID for a specific process
     @param envInput   Requested environment to load config parameters for
     @param apiUtility Object providing implementation of required Control Room API functions

     @return A360 Dictionary object containing loaded config parameters
     */
    public static DictionaryValue loadConfigFile(String processId, Environment envInput, ControlRoomApiUtility apiUtility)
            throws Exception
    {
        // validate environment
        Environment environment = validateEnvironment(apiUtility, envInput);

        // check if the fileshare root folder has been set and is valid
        boolean isFileshareRootFolderValid = validateFileshare(apiUtility);

        // load JSON from config file either from fileshare or from Control Room
        String configFileJson;

        // determine whether to load config file from fileshare or from CR repository
        if (isFileshareRootFolderValid)
        {
            // extract JSON from config file on fileshare
            configFileJson = getConfigJsonFromFileshare(processId, apiUtility);

        } else
        {
            // extract JSON from config file in CR repository
            configFileJson = getConfigJsonFromRepository(processId, apiUtility);
        }

        // map JSON into A360 objects and return as dictionary
        return processConfigJson(processId, environment, configFileJson, apiUtility);
    }

    /**
     Private method to validate the environment requested by a user against the Control Room's "strEnvironment" global value.
     If a user requests the "Default" environment, this is converted into the actual environment value. If the user requests to override the default and load a different
     environment, this override is applied only if the user is not trying to load values for PROD from a different environment.

     @param apiUtility Object providing implementation of required Control Room API functions
     @param envInput   Environment requested by user

     @return Environment to be used when extracting parameters from config file
     */
    private static Environment validateEnvironment(ControlRoomApiUtility apiUtility, Environment envInput)
            throws Exception
    {
        // get Control Room global value for environment
        Environment envGlobalValue = Environment.valueOf(apiUtility.getGlobalValueString("strEnvironment"));

        // validate environment
        if (envInput == Environment.PROD && envGlobalValue != Environment.PROD)
        {
            // user has made an invalid request to load PROD config parameters from a non-PROD environment - throw exception
            throw new Exception("Cannot override environment to load production config unless running in production Control Room!");

        } else if (envInput != Environment.PROD && envInput != Environment.DEFAULT)
        {
            // user has made a valid request to load config parameters from a different environment
            return envInput;

        } else
        {
            // user has requested the default/current environment
            return envGlobalValue;
        }
    }

    /**
     Private method to validate if a fileshare is identified and accessible.

     @param apiUtility Object providing implementation of required Control Room API functions

     @return true is fileshare is defined and accessible, false if not
     */
    private static boolean validateFileshare(ControlRoomApiUtility apiUtility)
    {
        // set output to false by default
        boolean isFileshareValid = false;

        try
        {
            // get global value for fileshare root folder, if available
            String fileshareRootFolder = apiUtility.getGlobalValueString("strFileshareRpaRootFolder");

            // get object for fileshare root folder
            File fileshareRootFolderObj = Paths.get(fileshareRootFolder).toFile();

            // check if this folder exists and represents a directory
            if (fileshareRootFolderObj.exists() && fileshareRootFolderObj.isDirectory())
            {
                // fileshare has been validated successfully - update output to true
                isFileshareValid = true;
            }
        } catch (Exception ex)
        {
            // no action required - leave boolean flag as false if all conditions are not met successfully
        }

        return isFileshareValid;
    }

    /**
     Private method to locate a config file for a specific process from a network fileshare and extract the content from it.

     @param processId  Unique ID for a specific process
     @param apiUtility Object providing implementation of required Control Room API functions

     @return Config file content
     */
    private static String getConfigJsonFromFileshare(String processId, ControlRoomApiUtility apiUtility)
            throws Exception
    {
        // get path to fileshare root folder from global value (existence of this global value is already validated)
        String fileshareRootFolder = apiUtility.getGlobalValueString("strFileshareRpaRootFolder");

        // get path to config subfolder within process-specific subfolder
        String pathToProcessConfigFolder = fileshareRootFolder + File.separator + processId + File.separator + "Config";
        Path processConfigFolderPath = Paths.get(pathToProcessConfigFolder);
        File processConfigFolder = processConfigFolderPath.toFile();

        // confirm that process config folder exists and is a directory
        if (!processConfigFolder.exists() || !processConfigFolder.isDirectory())
        {
            throw new Exception("Process config folder could not be found on the fileshare at " + pathToProcessConfigFolder);
        }

        // get files in this directory starting with the process ID and ending with ".json"
        List<Path> filesInProcessConfigFolder = Files.list(processConfigFolderPath)
                .filter(file -> !Files.isDirectory(file))
                .filter(path -> path.getName(path.getNameCount() - 1).toString().startsWith(processId))
                .filter(path -> path.getName(path.getNameCount() - 1).toString().endsWith("config.json"))
                .collect(Collectors.toList());

        // check results
        if (filesInProcessConfigFolder.size() < 1)
        {
            // no files found
            throw new Exception("Process config file could not be found on the fileshare - no matches found during search");
        } else if (filesInProcessConfigFolder.size() > 1)
        {
            // multiple files found
            throw new Exception("Process config file could not be found on the fileshare - multiple matches found during search");
        } else
        {
            // extract JSON from config file on fileshare
            return new String(Files.readAllBytes(filesInProcessConfigFolder.get(0)));
        }
    }

    /**
     Private method to locate a config file for a specific process in the Control Room repository and extract the content from it.

     @param processId  Unique ID for a specific process
     @param apiUtility Object providing implementation of required Control Room API functions

     @return Config file content
     */
    private static String getConfigJsonFromRepository(String processId, ControlRoomApiUtility apiUtility)
            throws Exception
    {
        // STEP 1: FIND "Automation Anywhere\Bots\Processes" FOLDER

        String processesFolderId = "";

        try
        {
            JSONObject processesFolderResponse = apiUtility.getItemsInWorkspace(WorkspaceType.PUBLIC, FILTER_REQUEST_PROCESSES_FOLDER);
            processesFolderId = processesFolderResponse.getJSONArray("list").getJSONObject(0).getString("id");
        } catch (Exception ex)
        {
            throw new Exception("Processes folder could not be found in the public repository - " + ex.getMessage());
        }


        // STEP 2: FIND PROCESS-SPECIFIC SUBFOLDER WITHIN "Automation Anywhere\Bots\Processes" FOLDER

        // find subfolder(s) matching process-specific subfolder
        String processSubfolderFilterRequest = FILTER_REQUEST_PROCESS_SUBFOLDER.replace("[PROCESS_ID]", processId).replace("[PROCESS_FOLDER_ID]", processesFolderId);
        JSONObject processSubfolderResponse = apiUtility.getItemsInWorkspace(WorkspaceType.PUBLIC, processSubfolderFilterRequest);

        // check how many folders were found and get process-specific folder ID
        String processSubfolderId = "";

        long countOfSearchResults = processSubfolderResponse.getJSONObject("page").getLong("totalFilter");

        if (countOfSearchResults < 1)
        {
            // process-specific folder was not found
            throw new Exception("Process-specific folder for process ID " + processId + " could not be found in the public repository - no matches found during search");

        } else
        {
            // scan results to confirm if a single correct process-specific subfolder is identified

            // create regex pattern
            String processSubfolderPatternInput = REGEX_PATTERN_PROCESS_SUBFOLDER.replace("PROCESS_ID", processId);
            Pattern processSubfolderPattern = Pattern.compile(processSubfolderPatternInput);
            Matcher processSubfolderMatcher = processSubfolderPattern.matcher("");

            // get list of results
            JSONArray processSubfolderResults = processSubfolderResponse.getJSONArray("list");

            // initialise counter to check number of possible matches
            int countOfPossibleMatches = 0;

            // loop through list of results
            for (Object processSubfolderResultObj : processSubfolderResults)
            {
                // cast to JSONObject
                JSONObject processSubfolderResult = (JSONObject) processSubfolderResultObj;

                // get path
                String processSubfolderResultPath = processSubfolderResult.getString("path");

                // check if regex match against path
                if (processSubfolderMatcher.reset(processSubfolderResultPath).find())
                {
                    // match successful - set ID and increment count
                    processSubfolderId = processSubfolderResult.getString("id");
                    countOfPossibleMatches++;
                }
            }

            // check if ID was set successfully - if not, throw exception
            if (countOfPossibleMatches > 1)
            {
                throw new Exception("Process-specific folder for process ID " + processId + " could not be isolated in the public repository - multiple potential matches found");
            }
        }


        // STEP 3: FIND "Assets" SUBFOLDER WITHIN PROCESS-SPECIFIC SUBFOLDER

        // get "Assets" subfolder using process-specific subfolder ID as parent ID
        String assetsSubfolderFilterRequest = FILTER_REQUEST_ASSETS_SUBFOLDER.replace("[PROCESS_SUBFOLDER_ID]", processSubfolderId);
        JSONObject assetsSubfolderResponse = apiUtility.getItemsInWorkspace(WorkspaceType.PUBLIC, assetsSubfolderFilterRequest);

        // check how many folders were found and get assets subfolder ID
        String assetsSubfolderId = "";

        countOfSearchResults = assetsSubfolderResponse.getJSONObject("page").getLong("totalFilter");

        if (countOfSearchResults < 1)
        {
            // assets subfolder was not found
            throw new Exception("Assets folder for process ID " + processId + " could not be found in the public repository - no matches found during search");

        } else
        {
            // scan results to confirm if a single correct Assets subfolder is identified

            // create regex pattern
            Pattern assetsSubfolderPattern = Pattern.compile(REGEX_PATTERN_ASSETS_SUBFOLDER);
            Matcher assetsSubfolderMatcher = assetsSubfolderPattern.matcher("");

            // get list of results
            JSONArray assetsResults = assetsSubfolderResponse.getJSONArray("list");

            // loop through list of results
            for (Object assetsSubfolderResultObj : assetsResults)
            {
                // cast to JSONObject
                JSONObject assetsSubfolderResult = (JSONObject) assetsSubfolderResultObj;

                // get path
                String assetsSubfolderResultPath = assetsSubfolderResult.getString("path");

                // check if regex match against path
                if (assetsSubfolderMatcher.reset(assetsSubfolderResultPath).find())
                {
                    // match successful - set ID and increment counter
                    assetsSubfolderId = assetsSubfolderResult.getString("id");
                    break;
                }
            }

            // check if ID was set successfully - if not, throw exception
            if (assetsSubfolderId.isEmpty())
            {
                throw new Exception("Assets subfolder for process ID " + processId + " could not be isolated in the public repository - at least one potential match found but no" +
                                            " exact match found");
            }
        }


        // STEP 4: FIND CONFIG FILE WITHIN ASSETS SUBFOLDER

        // find config file using assets subfolder ID as parent ID
        String configFileFilterRequest = FILTER_REQUEST_CONFIG_FILE_JSON.replace("[ASSETS_FOLDER_ID]", assetsSubfolderId);
        JSONObject configFileResponse = apiUtility.getItemsInWorkspace(WorkspaceType.PUBLIC, configFileFilterRequest);

        // check how many files were found and get config file ID
        String configFileId = "";

        countOfSearchResults = configFileResponse.getJSONObject("page").getLong("totalFilter");

        if (countOfSearchResults < 1)
        {
            // config file was not found
            throw new Exception("Config file for process ID " + processId + " could not be found in the public repository - no matches found during search");

        } else
        {
            // scan results to check if a single correct config file is identified

            // create regex pattern
            String configFilePatternInput = REGEX_PATTERN_CONFIG_FILE.replace("PROCESS_ID", processId).replace("ASSETS_FOLDER_ID", assetsSubfolderId);
            Pattern configFilePattern = Pattern.compile(configFilePatternInput);
            Matcher configFileMatcher = configFilePattern.matcher("");


            // get list of results
            JSONArray configFileResults = configFileResponse.getJSONArray("list");

            // initialise counter to check number of possible matches
            int countOfPossibleMatches = 0;

            // loop through list of results
            for (Object configFileResultObj : configFileResults)
            {
                // cast to JSONObject
                JSONObject configFileResult = (JSONObject) configFileResultObj;

                // get path
                String configFileResultPath = configFileResult.getString("path");

                // check if regex match against path
                if (configFileMatcher.reset(configFileResultPath).find())
                {
                    // match successful - set ID and increment counter
                    configFileId = configFileResult.getString("id");
                    countOfPossibleMatches++;
                }
            }

            // check if ID was set successfully - if not, throw exception
            if (countOfPossibleMatches > 1)
            {
                throw new Exception("Config file for process ID " + processId + " could not be isolated in the public repository - multiple potential matches found");
            }
        }


        // STEP 5: READ AND RETURN JSON FROM CONFIG FILE

        // get file content as JSON object
        JSONObject configFileObj = apiUtility.getFileContentAsJsonObject(Long.valueOf(configFileId));

        // convert to string
        return configFileObj.toString();
    }

    /**
     @param processId      Unique ID for a specific process
     @param environment    Environment to be used when extracting parameters from config file
     @param configFileJson Config file content
     @param apiUtility     Object providing implementation of required Control Room API functions

     @return A360 Dictionary
     */
    private static DictionaryValue processConfigJson(String processId, Environment environment, String configFileJson, ControlRoomApiUtility apiUtility)
            throws Exception
    {
        // convert entire config file to map
        Map<String, Value> inputMap = SerializationUtility.deserializeDictionary(configFileJson, false);

        // create output map
        Map<String, Value> outputMap = new LinkedHashMap<>();

        // add process ID
        outputMap.put("strProcessId", new StringValue(processId));

        // add local RPA root folder
        try
        {
            String strLocalRpaRootFolder = apiUtility.getGlobalValueString("strLocalRpaRootFolder");
            strLocalRpaRootFolder = ControlRoomApiUtility.replaceVariablisedPath(strLocalRpaRootFolder);
            outputMap.put("strProcessLocalRootFolder", new StringValue(strLocalRpaRootFolder + File.separator + processId));
        } catch (Exception ex)
        {
            // suppress exceptions
        }

        // add fileshare RPA root folder
        try
        {
            String strFileshareRpaRootFolder = apiUtility.getGlobalValueString("strFileshareRpaRootFolder");
            strFileshareRpaRootFolder = ControlRoomApiUtility.replaceVariablisedPath(strFileshareRpaRootFolder);
            outputMap.put("strProcessFileshareRootFolder", new StringValue(strFileshareRpaRootFolder + File.separator + processId));
        } catch (Exception ex)
        {
            // suppress exceptions
        }

        // get "common" element and add all values
        Map<String, Value> mapCommon = ((DictionaryValue) inputMap.get("common")).get();

        for (String key : mapCommon.keySet())
        {
            outputMap.put(key, mapCommon.get(key));
        }

        // get environment-specific element and add all values
        Map<String, Value> mapEnvironment = ((DictionaryValue) inputMap.get(environment.toString())).get();

        for (String key : mapEnvironment.keySet())
        {
            outputMap.put(key, mapEnvironment.get(key));
        }

        // add other variables
        return new DictionaryValue(outputMap);
    }
}
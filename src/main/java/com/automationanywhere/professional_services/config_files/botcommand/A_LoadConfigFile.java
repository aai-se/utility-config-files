package com.automationanywhere.professional_services.config_files.botcommand;

import com.automationanywhere.bot.service.BotException;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.config_files.ConfigFileLoader;
import com.automationanywhere.professional_services.config_files.Environment;
import com.automationanywhere.professional_services.utilities.ControlRoomApiUtility;

import java.util.Locale;

import static com.automationanywhere.commandsdk.model.DataType.ANY;

@BotCommand
@CommandPkg
        (
                name = "LoadConfigFile",
                label = "[[LoadConfigFile.label]]",
                node_label = "[[LoadConfigFile.node_label]]",
                description = "[[LoadConfigFile.description]]",
                icon = "pkg.svg",

                return_label = "[[LoadConfigFile.return_label]]",
                return_type = DataType.DICTIONARY,
                return_sub_type = ANY,
                return_required = true
        )
public class A_LoadConfigFile
{

    public static final String ENV_DEFAULT = "Default";
    public static final String ENV_DEV = "DEV";
    public static final String ENV_UAT = "UAT";
    public static final String ENV_PROD = "PROD";

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public DictionaryValue loadConfigFile(

            @Idx(index = "1", type = AttributeType.NUMBER)
            @Pkg(label = "[[LoadConfigFile.processId.label]]", default_value_type = DataType.NUMBER)
            @NotEmpty
                    Double processIdInput,

            @Idx(index = "2", type = AttributeType.SELECT, options = {
                    @Idx.Option(index = "2.1", pkg = @Pkg(label = "[[LoadConfigFile.environmentOverride.default.label]]", value = ENV_DEFAULT)),
                    @Idx.Option(index = "2.2", pkg = @Pkg(label = "[[LoadConfigFile.environmentOverride.dev.label]]", value = ENV_DEV)),
                    @Idx.Option(index = "2.3", pkg = @Pkg(label = "[[LoadConfigFile.environmentOverride.uat.label]]", value = ENV_UAT)),
                    @Idx.Option(index = "2.4", pkg = @Pkg(label = "[[LoadConfigFile.environmentOverride.prod.label]]", value = ENV_PROD))
            })
            @Pkg(label = "[[LoadConfigFile.environment.label]]", description = "[[LoadConfigFile.environment.description]]", default_value = ENV_DEFAULT,
                    default_value_type = DataType.STRING)
            @NotEmpty
                    String environmentInput

    )
    {
        try
        {
            // convert process ID from number to string
            String processId = Long.toString(processIdInput.longValue());

            // convert environment input to enum
            Environment environment = Environment.valueOf(environmentInput.toUpperCase(Locale.ROOT));

            // create REST client
            ControlRoomApiUtility apiUtility = new ControlRoomApiUtility(this.globalSessionContext);

            // load config file and return as JSON
            return ConfigFileLoader.loadConfigFile(processId, environment, apiUtility);

        } catch (Exception ex)
        {
            ex.printStackTrace();
            throw new BotException(ex);
        }
    }
}
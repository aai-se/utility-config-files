package com.automationanywhere.professional_services.config_files.testing;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.professional_services.config_files.botcommand.A_LoadConfigFile;

public class Test
{
    public static void main(String[] args)
    {
        try
        {
            A_LoadConfigFile command = new A_LoadConfigFile();

            Double processIdInput = 124D;
            String environmentInput = "UAT";

            DictionaryValue output = command.loadConfigFile(processIdInput, environmentInput);
            String breakpoint = "";
        }
        catch (Exception ex)
        {
            String breakpoint = ex.getMessage();
        }
    }

}

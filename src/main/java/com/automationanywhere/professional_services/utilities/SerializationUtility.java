package com.automationanywhere.professional_services.utilities;

import com.automationanywhere.botcommand.data.Value;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 This class is responsible for providing (de)serialization functionality of A360 Dictionary objects for use by other classes.
 */
public class SerializationUtility
{

    /**
     Method to serialize a map of A360 values to a JSON string.

     * @param inputMap Map to be serialized
     * @param convertToCustomFormat boolean flag to indicate whether custom formatting should be used
     * @return String containing JSON representation of input map
     * @throws Exception
     */
    public static String serializeDictionary(Map<String, Value> inputMap, Boolean convertToCustomFormat)
            throws Exception
    {
        // create new map to hold converted values
        Map<String, Object> outputMap = new HashMap<>();

        Integer index = 0;

        // loop through values
        for (Map.Entry<String, Value> entry : inputMap.entrySet())
        {
            // create ValueObjectMapping for this value
            ValueObjectMapping mapping = new ValueObjectMapping(index, entry.getKey(), entry.getValue(), convertToCustomFormat);

            // add value mapping to output map
            if (convertToCustomFormat)
            {
                outputMap.put(mapping.getKeyInCustomFormat(), mapping.obj);
            } else
            {
                outputMap.put(mapping.name, mapping.obj);
            }

            // increment index
            index++;
        }

        // create new JSONObject using output map
        JSONObject jsonObject = new JSONObject(outputMap);
        return jsonObject.toString();
    }

    /**
    Method to deserialize a JSON string to a map of A360 values.

     * @param json String containing JSON representation of input map
     * @param convertToCustomFormat boolean flag to indicate whether custom formatting should be used
     * @return Map to be used in creating an A360 Dictionary object
     * @throws Exception
     */
    public static Map<String, Value> deserializeDictionary(String json, Boolean convertToCustomFormat)
            throws Exception
    {
        // get input map from JSONObject
        Map<String, Object> inputMap = new JSONObject(json).toMap();

        // instantiate output map
        Map<String, Value> outputMap = new LinkedHashMap<>();

        // if using custom formatting, apply logic to restore types and ordering
        if (convertToCustomFormat)
        {
            // instantiate TreeMap object to perform automatic sorting/ordering of keys
            TreeMap<String, ValueObjectMapping> sortedMap = new TreeMap<>();

            // loop through values to add to sorted map using object index as key
            for (Map.Entry<String, Object> entry : inputMap.entrySet())
            {
                // create ValueObjectMapping for this object
                ValueObjectMapping mapping = new ValueObjectMapping(entry.getKey(), entry.getValue(), convertToCustomFormat);

                // add mapping to sorted map to restore correct ordering
                sortedMap.put(mapping.index.toString(), mapping);
            }

            // loop through sorted map to add values to output map
            for (Map.Entry<String, ValueObjectMapping> entry : sortedMap.entrySet())
            {
                // get ValueObjectMapping
                ValueObjectMapping mapping = entry.getValue();

                // add value to output map
                outputMap.put(mapping.name, mapping.val);
            }
        } else
        {
            // loop through input map to add values to output map
            for (Map.Entry<String, Object> entry : inputMap.entrySet())
            {
                // create ValueObjectMapping for this object
                ValueObjectMapping mapping = new ValueObjectMapping(null, entry.getValue(), convertToCustomFormat);

                // add value to output map
                outputMap.put(entry.getKey(), mapping.val);
            }
        }

        return outputMap;
    }
}
package com.automationanywhere.professional_services.utilities;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.enums.SessionType;
import com.automationanywhere.professional_services.cr_apis.apis.enums.WorkspaceType;
import com.automationanywhere.professional_services.cr_apis.apis.functions._private_v1_GlobalValues;
import com.automationanywhere.professional_services.cr_apis.apis.functions._private_v2_RepositoryManagement;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_RepositoryManagement;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.sun.jna.platform.win32.WinReg;
import org.json.JSONArray;
import org.json.JSONObject;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 This class is responsible for providing Control Room API functionality for use by other classes.
 */
public class ControlRoomApiUtility
{
    // string for dynamic mapping of Windows environment variable for user profile
    public static final String PLACEHOLDER_USERPROFILE = "%USERPROFILE%";

    // string for dynamic mapping of Windows registry value for default user folder in Windows Explorer
    public static final String PLACEHOLDER_SHELL_FOLDERS_PERSONAL = "[REGISTRY_SHELL_FOLDERS_PERSONAL]";

    // client for performing Control Room REST API calls
    private ControlRoomRestClient client;

    // map to use when storing global values
    private Map<String, Value> globalValues;

    /**
     Constructor for use inside a bot agent session.

     @param globalSessionContext Bot agent GlobalSessionContext object
     */
    public ControlRoomApiUtility(GlobalSessionContext globalSessionContext)
            throws Exception
    {
        // configure client
        this.client = new ControlRoomRestClient(SessionType.BOT_RUNNER_SESSION, globalSessionContext, null, null, null, null);

        // connect client
        this.client.updateToken();
    }

    /**
     Constructor for use with custom authentication.

     @param controlRoomUrl Control Room URL
     @param username       Username to authenticate with
     @param authMethod     Method of authentication - must be either "PASSWORD" or "API_KEY"
     @param authInput      Authentication input - must correlate to
     */
    public ControlRoomApiUtility(String controlRoomUrl, String username, String authMethod, String authInput)
            throws Exception
    {
        // configure client
        this.client = new ControlRoomRestClient(SessionType.CUSTOM_SESSION, null, controlRoomUrl, username, authMethod, authInput);

        // connect client
        this.client.updateToken();
    }

    // GLOBAL VALUES

    /**
     Method to replace placeholder values in a path with dynamic values obtained at runtime.

     @param path Path to be updated

     @return Updated path with placeholder values replaced with dynamic values
     */
    public static String replaceVariablisedPath(String path)
    {
        if (path.contains(PLACEHOLDER_USERPROFILE))
        {
            // replace environment variable placeholder with its value
            path = path.replace(PLACEHOLDER_USERPROFILE, System.getenv("USERPROFILE"));

        } else if (path.contains(PLACEHOLDER_SHELL_FOLDERS_PERSONAL))
        {
            // read value from registry
            WinReg.HKEY root = WinReg.HKEY_CURRENT_USER;
            String key = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders";
            String value = "Personal";

            String valueOutput = RegistryUtilities.getRegistryValue(root, key, value);

            // replace registry key placeholder with its value
            path = path.replace(PLACEHOLDER_SHELL_FOLDERS_PERSONAL, valueOutput);
        }

        return path;
    }

    /**
     Method to get a single global value from the Control Room.

     @param key Global value key

     @return Global value
     */
    public Value getGlobalValue(String key)
            throws Exception
    {
        // ensure that global values have been populated already; if not, update them
        if (this.globalValues == null)
        {
            updateGlobalValues();
        }

        // return single global value
        return this.globalValues.get(key);
    }

    /**
     Method to get all global value from the Control Room.

     @return All global values
     */
    public Map<String, Value> getAllGlobalValues()
            throws Exception
    {
        // ensure that global values have been populated already; if not, update them
        if (this.globalValues == null)
        {
            updateGlobalValues();
        }

        // return all global values
        return this.globalValues;
    }

    /**
     Convenience method for getting a global value as a string.

     @param key Global value key

     @return Global value as string
     */
    public String getGlobalValueString(String key)
            throws Exception
    {
        return ((StringValue) this.getGlobalValue(key)).get();
    }

    /**
     Method to update internal map with global values from Control Room.
     */
    public void updateGlobalValues()
            throws Exception
    {
        // instantiate output variable
        this.globalValues = new HashMap<>();

        // call CR API to get global values
        ApiResponse response = _private_v1_GlobalValues.getAllGlobalValues(client);

        // get list of global values as JSONArray
        JSONArray globalValuesListObj = response.getJsonObject().getJSONArray("list");

        // iterate through list to compose values
        for (int i = 0; i < globalValuesListObj.length(); i++)
        {
            // get object from array
            JSONObject globalValueObj = (JSONObject) globalValuesListObj.get(i);

            // get name
            String key = globalValueObj.getString("name");

            // get type
            String type = globalValueObj.getString("type");

            // get value as JSONObject
            JSONObject valueDefinition = (JSONObject) globalValueObj.get("value");

            // get value depending on type
            switch (type)
            {
                case "STRING":
                    this.globalValues.put(key, new StringValue(valueDefinition.getString("string")));
                    break;
                case "NUMBER":
                    this.globalValues.put(key, new NumberValue(valueDefinition.getString("number")));
                    break;
                case "BOOLEAN":
                    this.globalValues.put(key, new BooleanValue(valueDefinition.getString("boolean")));
                    break;
                case "DATETIME":
                    this.globalValues.put(key, new DateTimeValue(ZonedDateTime.parse(valueDefinition.getString("string"))));
                    break;
                default:
                    throw new Exception("Did not match global value type correctly - type was " + type);
            }
        }
    }

    // FILES

    /**
     Method to find items in a workspace.

     @param workspaceType     Public or private
     @param filterRequestJson Filter request

     @return JSONObject with results
     */
    public JSONObject getItemsInWorkspace(WorkspaceType workspaceType, String filterRequestJson)
            throws Exception
    {
        return v2_RepositoryManagement.getItemsInWorkspace(this.client, workspaceType, filterRequestJson).getJsonObject();
    }

    /**
     Method to find information for a specific file.

     @param fileId File ID

     @return JSONObject with file info
     */
    public JSONObject getFileInfo(Long fileId)
            throws Exception
    {
        return _private_v2_RepositoryManagement.getFileInfo(this.client, fileId).getJsonObject();
    }

    /**
     Method to get content of a specific file as JSON.

     @param fileId File ID

     @return JSONObject representing file content
     */
    public JSONObject getFileContentAsJsonObject(long fileId)
            throws Exception
    {
        return _private_v2_RepositoryManagement.getBotContent(this.client, fileId).getJsonObject();
    }
}
package com.automationanywhere.professional_services.utilities;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

/**
 Class to be used in mapping A360 values to objects.
 */
public class ValueObjectMapping
{
    public static final String PATTERN_LOCAL_DATE_TIME = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}$";

    public final Integer index;
    public final VariableType type;
    public final String name;
    public final Boolean convertToCustomFormat;
    public final Object obj;
    public final Value val;

    public ValueObjectMapping(Integer index, String name, Value inputValue, Boolean convertToCustomFormat)
            throws Exception
    {
        // store index, name and flag for converting to custom format
        this.index = index;
        this.name = name;
        this.convertToCustomFormat = convertToCustomFormat;

        if (inputValue instanceof StringValue)
        {
            this.type = VariableType.STR;
            this.val = inputValue;
            this.obj = this.val.get();

        } else if (inputValue instanceof NumberValue)
        {
            this.type = VariableType.NUM;
            this.val = inputValue;
            this.obj = this.val.get();

        } else if (inputValue instanceof BooleanValue)
        {
            this.type = VariableType.BOOL;
            this.val = inputValue;
            this.obj = this.val.get();

        } else if (inputValue instanceof DateTimeValue)
        {
            this.type = VariableType.DATE;
            this.val = inputValue;
            this.obj = this.val.get().toString();

        } else if (inputValue instanceof DictionaryValue)
        {
            this.type = VariableType.DICT;
            this.val = inputValue;

            DictionaryValue valueObj = (DictionaryValue) inputValue;
            LinkedHashMap<String, Value> valueMap = (LinkedHashMap<String, Value>) valueObj.get();

            Map<String, Object> objectMap = new LinkedHashMap<>();

            int childIndex = 0;

            for (Map.Entry<String, Value> entry : valueMap.entrySet())
            {
                ValueObjectMapping mapping = new ValueObjectMapping(childIndex, entry.getKey(), entry.getValue(), convertToCustomFormat);

                // add value mapping to output map
                if (convertToCustomFormat)
                {
                    objectMap.put(mapping.getKeyInCustomFormat(), mapping.obj);
                } else
                {
                    objectMap.put(mapping.name, mapping.obj);
                }

                childIndex++;
            }

            this.obj = objectMap;

        } else if (inputValue instanceof ListValue)
        {
            this.type = VariableType.LIST;
            this.val = inputValue;

            ListValue valueObj = (ListValue) this.val;

            List<Value> valueList = valueObj.get();

            if (convertToCustomFormat)
            {
                Map<String, Object> objectMap = new LinkedHashMap<>();

                int childIndex = 0;

                for (Value item : valueList)
                {
                    ValueObjectMapping mapping = new ValueObjectMapping(childIndex, "listItem", item, convertToCustomFormat);
                    objectMap.put(mapping.getKeyInCustomFormat(), mapping.obj);
                    childIndex++;
                }

                this.obj = objectMap;
            } else
            {
                List<Object> objectList = new ArrayList<>();

                for (Value item : valueList)
                {
                    ValueObjectMapping mapping = new ValueObjectMapping(null, null, item, convertToCustomFormat);
                    objectList.add(mapping.obj);
                }

                this.obj = objectList;
            }


        } /*

        // THIS WILL TURN MY BRAIN INTO SWISS CHEESE
        // BUT IT CAN BE DONE!!

        else if (input instanceof TableValue)
        {
            this.keyPrefix = "TBL|";

            TableValue valueObj = (TableValue) input;
            Table tableObj = valueObj.get();
            List<Schema> schemas = tableObj.getSchema();
            Schema schema = schemas.get(0);
            schema.

        } */ else
        {
            throw new Exception("Value type not supported - " + inputValue.getClass().toString());
        }
    }

    public ValueObjectMapping(String key, Object inputObj, Boolean convertToCustomFormat)
            throws Exception
    {
        // store flag for converting to custom format
        this.convertToCustomFormat = convertToCustomFormat;

        // store input object
        this.obj = inputObj;

        // declare collection variables
        Map<String, Value> inputMap, outputMap;
        Map<String, ValueObjectMapping> sortedMap;
        List<ValueObjectMapping> inputList;
        List<Value> outputList;

        // populate variables if using a custom format
        if (convertToCustomFormat)
        {
            // split key into constituent parts
            String[] keySplit = key.split("\\|");

            // store individual metadata variables from key
            this.index = Integer.valueOf(keySplit[0]);
            this.type = VariableType.valueOf(keySplit[1]);
            this.name = (keySplit[2]);

            switch (this.type)
            {
                case STR:

                    this.val = new StringValue((String) this.obj);
                    break;

                case NUM:

                    this.val = new NumberValue(this.obj);
                    break;

                case BOOL:

                    this.val = new BooleanValue((Boolean) this.obj);
                    break;

                case DATE:

                    this.val = new DateTimeValue(ZonedDateTime.parse((String) this.obj));
                    break;

                case DICT:

                    // get/create maps for input, sorting and output
                    inputMap = (HashMap) this.obj;
                    sortedMap = new TreeMap<>();
                    outputMap = new LinkedHashMap<>();

                    // create mapping objects from input map and add to sorted map
                    for (Map.Entry<String, Value> entry : inputMap.entrySet())
                    {
                        ValueObjectMapping mapping = new ValueObjectMapping(entry.getKey(), entry.getValue(), convertToCustomFormat);
                        sortedMap.put(mapping.index.toString(), mapping);
                    }

                    // add elements from sorted map to output map
                    for (Map.Entry<String, ValueObjectMapping> entry : sortedMap.entrySet())
                    {
                        ValueObjectMapping mapping = entry.getValue();
                        outputMap.put(mapping.name, mapping.val);
                    }

                    this.val = new DictionaryValue(outputMap);
                    break;

                case LIST:

                    // instantiate output object
                    outputList = new ArrayList<>();

                    // instantiate input and sorting collection objects
                    inputMap = (HashMap) this.obj;
                    sortedMap = new TreeMap<>();

                    // create mapping objects from input map and add to sorted map
                    for (Map.Entry<String, Value> entry : inputMap.entrySet())
                    {
                        ValueObjectMapping mapping = new ValueObjectMapping(entry.getKey(), entry.getValue(), convertToCustomFormat);
                        sortedMap.put(mapping.index.toString(), mapping);
                    }

                    // add elements from sorted map to output list
                    for (Map.Entry<String, ValueObjectMapping> entry : sortedMap.entrySet())
                    {
                        ValueObjectMapping mapping = entry.getValue();
                        outputList.add(mapping.val);
                    }

                    ListValue output = new ListValue();
                    output.set(outputList);
                    this.val = output;
                    break;

                default:
                    throw new Exception("Unsupported variable type");
            }

        } else
        {
            this.index = null;
            this.type = null;
            this.name = null;

            if (this.obj instanceof String)
            {
                // cast to string
                String inputString = (String) this.obj;

                // test if string matches local date time pattern
                if (inputString.matches(PATTERN_LOCAL_DATE_TIME))
                {
                    // convert string to local datetime
                    LocalDateTime localDateTime = LocalDateTime.parse(inputString);

                    // convert local date time to zoned datetime using local/default datetime
                    ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());

                    // convert zoned datetime to A360 value
                    this.val = new DateTimeValue(zonedDateTime);
                } else
                {
                    this.val = new StringValue((String) this.obj);
                }

            } else if (this.obj instanceof Number)
            {
                this.val = new NumberValue(this.obj);

            } else if (this.obj instanceof Boolean)
            {
                this.val = new BooleanValue(this.obj);

            } else if (this.obj instanceof Map)
            {
                inputMap = (HashMap) this.obj;
                outputMap = new LinkedHashMap<>();

                // directly insert items from input map to output map
                for (Map.Entry<String, Value> entry : inputMap.entrySet())
                {
                    ValueObjectMapping mapping = new ValueObjectMapping(null, entry.getValue(), convertToCustomFormat);
                    outputMap.put(entry.getKey(), mapping.val);
                }

                this.val = new DictionaryValue(outputMap);

            } else if (this.obj instanceof List)
            {
                // instantiate input and output objects
                inputList = (List) this.obj;
                outputList = new ArrayList<>();

                // create mapping objects from input list and add to output list
                for (Object listObj : inputList)
                {
                    ValueObjectMapping mapping = new ValueObjectMapping(null, listObj, convertToCustomFormat);
                    outputList.add(mapping.val);
                }

                ListValue output = new ListValue();
                output.set(outputList);
                this.val = output;
            } else
            {
                throw new Exception("Unsupported object type");
            }
        }


    }

    public String getKeyInCustomFormat()
    {
        return this.index + "|" + this.type.toString() + "|" + this.name;
    }

    public enum VariableType
    {
        STR, NUM, BOOL, DATE, DICT, LIST
    }

}

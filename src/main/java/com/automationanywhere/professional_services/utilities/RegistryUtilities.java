package com.automationanywhere.professional_services.utilities;

import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;

/**
 This class is responsible for providing Windows registry functionality for use by other classes.
 */
public class RegistryUtilities
{

    /**
     Method to obtain a single registry value.

     @param root  Registry root key
     @param key   Registry key
     @param value Registry value

     @return Data for registry value
     */
    public static String getRegistryValue(WinReg.HKEY root, String key, String value)
    {
        return Advapi32Util.registryGetStringValue(root, key, value);
    }
}